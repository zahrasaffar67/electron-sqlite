const {app, BrowserWindow, ipcMain} = require("electron");
const path = require('path');
const url = require("url");

$('#btnNext').on('click', function(){
    win = new BrowserWindow();
    win.loadURL(url.format({
     pathname: path.join(__dirname,'next.html'),
     protocol: 'file',
     slashes: true
 }));

})