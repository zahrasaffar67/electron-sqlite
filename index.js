const {app, BrowserWindow, ipcMain, globalShortcut} = require("electron");
const path = require('path');
const url = require("url");

var knex = require("knex")({
    client: "sqlite3",
    connection:{
        filename: "./Maskan-sqlite.db"
    }
    
    })

let win;

function createWindow(){
 win = new BrowserWindow({
    width: 800,
    height: 800,
    webPreferences: {
        nodeIntegration: true
    }
});
globalShortcut.register('f5', function() {
    console.log('f5 is pressed')
    win.reload()
})
 win.webContents.openDevTools();
 win.loadURL(url.format({
     pathname: path.join(__dirname,'main.html'),
     protocol: 'file',
     slashes: true
 }));

 ipcMain.on("mainWindowLoaded", function(){
    let result = knex.select("Person_name").from('Persons')
    result.then(function(rows){
       win.webContents.send('resultSent', rows)
    })
})

 win.on('close', () =>{
     win = null
 })
}

app.on('ready', createWindow);
